from flask import Flask, render_template
import sqlite3
app = Flask(__name__)

lr = {}
con = sqlite3.connect("igs.db")
cur = con.cursor()
cur.execute("SELECT seq,response,whn FROM lifthistory")
for (seq,response,whn) in cur:
    lr[seq] = lr[seq] if seq in lr else []
    lr[seq].append([response,whn])

lc = []
cur.execute("SELECT ttg, seq, command, location, offset FROM liftcmd ORDER BY ttg")
for (ttg,seq,command,location,offset) in cur:
    lc.append([ttg,seq,command,location,offset])

print(lc)

def mkRow(l):
    return "".join([f"<td>{e}</td>" for e in l])

def lastCmd(d):
    for i in range(1,len(lc)):
        if lc[i][0] > d:
            lastResponse = lr[lc[i-1][1]][-1]
            return [lastResponse,
                    lastResponse[0] == 'Done' and \
                            lastResponse[1] <= d
                            ]
    return 'failed'

def mkRowTrial(l):
    tds = [f"<td><img src='{l[0]}' style='width:100px'/></td>"] +\
          [f"<td>{lastCmd(l[1])}</td>"] + \
          [f"<td>{e}</td>" for e in l[1:]]
    return "".join(tds)


def mkTable(ll, rowFun = mkRow):
    trs = [f"<tr>{rowFun(l)}</tr>" for l in ll]
    return '<table>\n' + ("\n".join(trs)) + '</table>\n'

@app.route('/')
def index():
    con = sqlite3.connect("igs.db")
    cur = con.cursor()
    cur.execute("SELECT id,suc,startTime,crop FROM trial")
    def mkRowIndex(l):
        tds = [f"<td><a href='trial/{l[0]}'>{l[0]}</a></td>"] +\
              [f"<td>{e}</td>" for e in l[1:]]
        return "".join(tds)
    return mkTable(list(cur), rowFun = mkRowIndex)

@app.route('/trial/<int:t>')
def trial(t):
    con = sqlite3.connect("igs.db")
    cur = con.cursor()
    cur.execute("SELECT url,whn,offset FROM trialimage WHERE trial = ?",
            (t,))
    return mkTable(list(cur), rowFun = mkRowTrial)

if __name__ =='__main__':
    app.run('0.0.0.0', port = 5036, debug = True)
